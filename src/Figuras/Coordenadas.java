/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Figuras;

/**
 *
 * @author Grupo2
 */
public class Coordenadas {
    
    private int x;
    private int y;
    
    public Coordenadas(int x, int y){
        this.x = x;
        this.y = y;
    }
    
    public int getCoordX(){
        return this.x;
    }
    
    public int getCoordY(){
        return this.y;
    }
    
    public void setCoordX(int coordX){
        this.x = coordX;
    }
    
    public void setCoordY(int coordY){
        this.y = coordY;
    }
    
}
