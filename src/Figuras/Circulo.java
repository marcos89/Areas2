/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Figuras;

/**
 *
 * @author Grupo2
 */
public class Circulo extends FigurasGeometricas {
    
    private float radio;
    
    public Circulo(float radio){
        this.radio = radio;
    }
    
    public float getRadio(){
        return this.radio;
    }
    
    public void setRadio(int radio){
        this.radio = radio;
    }

    @Override
    public String toString() {
        return "Circulo{" + "radio=" + radio + '}';
    }
    
    @Override
    public float calcularArea(){
        float area = (float) (Math.PI*Math.pow(this.radio, 2));
        return area;
    }
    
}
