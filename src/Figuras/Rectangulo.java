/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Figuras;

/**
 *
 * @author Grupo2
 */
public class Rectangulo extends FigurasGeometricas{
    
    private float base;
    private float altura;
    
    public Rectangulo(float base, float altura){
        this.base = base;
        this.altura = altura;
    }
    
    public float getBase(){
        return this.base;
    }
    
    public float getAltura(){
        return this.altura;
    }
    
    public void setBase(int base){
        this.base = base;
    }
    
    public void setAltura(int altura){
        this.altura = altura;
    }

    @Override
    public String toString() {
        return "Rectangulo{" + "base=" + base + ", altura=" + altura + '}';
    }
    
    @Override
    public float calcularArea(){
        float area = this.base*this.altura;
        return area;
    }
    
}
