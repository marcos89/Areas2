/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package areas;

import Figuras.Circulo;

/**
 *
 * @author Grupo2
 */
public class AreaCirculo extends javax.swing.JFrame {

    /**
     * Creates new form AreaCirculo
     */
    public AreaCirculo() {
        initComponents();
        this.setLocationRelativeTo(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblTituloCirc = new javax.swing.JLabel();
        lblRadioCirc = new javax.swing.JLabel();
        lblResultadoCirc = new javax.swing.JLabel();
        txtRadioCirc = new javax.swing.JTextField();
        btnCirc = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        lblTituloCirc.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        lblTituloCirc.setText("Círculo");

        lblRadioCirc.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblRadioCirc.setText("Radio");

        txtRadioCirc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtRadioCircActionPerformed(evt);
            }
        });

        btnCirc.setText("Calcular area");
        btnCirc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCircActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(158, 158, 158)
                        .addComponent(lblTituloCirc))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnCirc)
                            .addComponent(lblRadioCirc))
                        .addGap(60, 60, 60)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtRadioCirc, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblResultadoCirc, javax.swing.GroupLayout.PREFERRED_SIZE, 182, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(18, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(lblTituloCirc)
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtRadioCirc, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblRadioCirc))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 82, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnCirc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblResultadoCirc, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(77, 77, 77))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtRadioCircActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtRadioCircActionPerformed
       
    }//GEN-LAST:event_txtRadioCircActionPerformed

    private void btnCircActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCircActionPerformed
        Float radio = Float.parseFloat(this.txtRadioCirc.getText());
        
        Circulo circulo = new Circulo(radio);
        
        lblResultadoCirc.setText("El area del círculo es " + circulo.calcularArea());
    }//GEN-LAST:event_btnCircActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCirc;
    private javax.swing.JLabel lblRadioCirc;
    private javax.swing.JLabel lblResultadoCirc;
    private javax.swing.JLabel lblTituloCirc;
    private javax.swing.JTextField txtRadioCirc;
    // End of variables declaration//GEN-END:variables
}
